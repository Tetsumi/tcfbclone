tcfbclone
=====================

A clone of Flappy Bird written in C.

![Screenshot](https://abload.de/img/screen49rye.png)

How to play
---------------------

Press space to avoid pipes.

How to build
---------------------

Execute `make.bash`.

How to use
----------------------

Execute `.\bin\tcfbclone.out`.

Dependencies
----------------------

- SDL2
- SDL2_ttf
- SDL2_mixer
- SDL2_image

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png)
