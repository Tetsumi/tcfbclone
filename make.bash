#!/bin/bash

nd='-DNDEBUG'

if [ "${1+defined}" ] && [ $1 = "-d" ]; then
    nd="-g"
fi

gcc `sdl2-config --cflags --libs` -lSDL2_ttf -lSDL2_image -lSDL2_mixer -O3 ${nd} ./source/*.c -o ./bin/tcfbclone.out 
