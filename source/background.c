/*
  tcfbclone
 
  Contributors:
  Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "base.h"

// logic screen is 640x480.
#define GROUND_Y 320
#define GROUND_HEIGHT 54
#define WATER_Y (GROUND_Y + GROUND_HEIGHT)
#define WATER_HEIGHT (480 - WATER_Y)

static P_SDL_Texture s_tex;

static C_SDL_Rect s_ulbg[] = {
	// UP
	[0] = {.x = 0, .y = 0, .w = 640, .h = GROUND_Y},
	// WATER
	[1] = {.x = 0, .y = WATER_Y, .w = 640, .h = WATER_HEIGHT},	
};

static Void renderGround(S32 offset)
{
        offset %= -216;
	for (S32 i = 0; i < 3; ++i)
		for (S32 j = 0; j < 4; ++j)
			CATALOG_renderTexture(TID_GROUND + i,
					      offset + (216 * j),
					      GROUND_Y + (i * 18));
}

static Void renderParallaxUP(S32 offset)
{
	for (S32 i= 0; i < 6; ++i)
	{
		C_S32 y = GROUND_Y - ((6 - i) * 30);
		C_S32 off = (offset / (6 - i)) % -240;
		for (S32 j = 0; j < 4; ++j)
		{
			CATALOG_renderTexture(TID_PARALLAX_1 + i,
					      off + (240 * j),
					      y);
			// buildings
		        switch (i)
			{
			case 3:
				CATALOG_renderTexture(TID_BUILDINGS_2,
						      off + (240 * j),
						      y - 20);
				CATALOG_renderTexture(TID_BUILDINGS_1,
						      off + 120 + (240 * j),
						      y - 20);
				break;
			case 4:
				CATALOG_renderTexture(TID_BUILDINGS_1,
						      off + (240 * j),
						      y - 5);
				CATALOG_renderTexture(TID_BUILDINGS_2,
						      off + 120 + (240 * j),
						      y - 5);
				break;
			
			}
		} 
	}
}

static Void renderParallaxWater(S32 offset)
{

	for (S32 i= 0; i < 6; ++i)
	{
		C_S32 y = WATER_Y + ((5 - i) * 10);
		C_S32 off = (offset / (6 - i)) % -240;
		for (S32 j = 0; j < 4; ++j)
		{
			CATALOG_renderTexture(TID_PARALLAX_WATER_1 + i,
					      off + (240 * j),
					      y);
			// buildings
		        switch (i)
			{
			case 3:
				CATALOG_renderTexture(TID_BUILDINGS_WATER_2,
						      off + (240 * j),
						      y + 20);
				CATALOG_renderTexture(TID_BUILDINGS_WATER_1,
						      off + 120 + (240 * j),
						      y + 20);
				break;
			case 4:
				CATALOG_renderTexture(TID_BUILDINGS_WATER_1,
						      off + (240 * j),
						      y + 13);
				CATALOG_renderTexture(TID_BUILDINGS_WATER_2,
						      off + 120 + (240 * j),
						      y + 13);
				break;
			
			}
		}
	}
	
}

static Void renderClouds(S32 offset)
{
	for (S32 i= 0; i < 2; ++i)
	{
		C_S32 off = (offset / (i + 1)) % -240;
		for (S32 j = 0; j < 4; ++j)
			CATALOG_renderTexture(TID_CLOUD_1 + i,
					      off + (i * 130) + (240 * j),
					      30 + (60 * i));
	}
}

Error BACKGROUND_render(C_S32 offset)
{
        // background UP
	SDL_SetRenderDrawColor(g_state.renderer, 12, 95, 218, 255);
	SDL_RenderFillRect(g_state.renderer, &s_ulbg[0]);	
	// background LOWER
	SDL_SetRenderDrawColor(g_state.renderer, 50, 18, 231, 255);
	SDL_RenderFillRect(g_state.renderer, &s_ulbg[1]);
	renderParallaxUP(offset); 
	renderGround(offset);
	renderParallaxWater(offset);
	renderClouds(offset);
	return ERROR_NONE;
}
