/*
  cdptlib
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <assert.h>
#include <SDL.h>
#include <SDL_mixer.h>

#define DEFINE_TYPEDEFS(Type)				\
	typedef Type const C_## Type;			\
	typedef Type *P_## Type;			\
	typedef Type const *PC_## Type;			\
	typedef Type * const CP_## Type;		\
	typedef Type const * const CPC_## Type;		\
	typedef Type **PP_## Type;			\
	typedef Type const **PPC_## Type;		\
	typedef Type ** const CPP_## Type;		\
	typedef Type * const * PCP_## Type;		\
	typedef Type * const * const CPCP_## Type;	\
	typedef Type const * const * PCPC_## Type;	\
	typedef Type const ** const CPPC_## Type;	\
	typedef Type const * const * const CPCPC_## Type;

typedef   int8_t  S8;
typedef  int16_t S16;
typedef  int32_t S32;
typedef  int64_t S64;

typedef  uint8_t  U8;
typedef uint16_t U16;
typedef uint32_t U32;
typedef uint64_t U64;

typedef    float R32;
typedef   double R64;

typedef   size_t Size;
typedef   size_t Index;

typedef     char Char;
typedef     void Void;
typedef     bool Boolean;
typedef    void* Address;

typedef      int Int;
typedef    float Float;

DEFINE_TYPEDEFS(S8);
DEFINE_TYPEDEFS(S16);
DEFINE_TYPEDEFS(S32);
DEFINE_TYPEDEFS(S64);

DEFINE_TYPEDEFS(U8);
DEFINE_TYPEDEFS(U16);
DEFINE_TYPEDEFS(U32);
DEFINE_TYPEDEFS(U64);

DEFINE_TYPEDEFS(R32);
DEFINE_TYPEDEFS(R64);

DEFINE_TYPEDEFS(Size);
DEFINE_TYPEDEFS(Index);

DEFINE_TYPEDEFS(Char);
DEFINE_TYPEDEFS(Void);
DEFINE_TYPEDEFS(Boolean);
DEFINE_TYPEDEFS(Address);

DEFINE_TYPEDEFS(Int);
DEFINE_TYPEDEFS(Float);

typedef  S8  VectorS8 __attribute__ ((vector_size (16)));
typedef S16 VectorS16 __attribute__ ((vector_size (16)));
typedef S32 VectorS32 __attribute__ ((vector_size (16)));
typedef S64 VectorS64 __attribute__ ((vector_size (16)));

typedef  U8  VectorU8 __attribute__ ((vector_size (16)));
typedef U16 VectorU16 __attribute__ ((vector_size (16)));
typedef U32 VectorU32 __attribute__ ((vector_size (16)));
typedef U64 VectorU64 __attribute__ ((vector_size (16)));

typedef R32 VectorR32 __attribute__ ((vector_size (16)));
typedef R64 VectorR64 __attribute__ ((vector_size (16)));


DEFINE_TYPEDEFS(VectorS8);
DEFINE_TYPEDEFS(VectorS16);
DEFINE_TYPEDEFS(VectorS32);
DEFINE_TYPEDEFS(VectorS64);

DEFINE_TYPEDEFS(VectorU8);
DEFINE_TYPEDEFS(VectorU16);
DEFINE_TYPEDEFS(VectorU32);
DEFINE_TYPEDEFS(VectorU64);

DEFINE_TYPEDEFS(VectorR32);
DEFINE_TYPEDEFS(VectorR64);

typedef enum
{
	ERROR_NONE,
	ERROR_UNKNOWN,
	ERROR_MEMORY,
	ERROR_OPEN_FILE,
	ERROR_NOT_IMPLEMENTED,
	ERROR_BAD_ARGUMENT,
	ERROR_SYSCALL
} Error;

DEFINE_TYPEDEFS(Error);

typedef Size Length;

DEFINE_TYPEDEFS(Length);

#define CLEANUP(function) __attribute__ ((__cleanup__(function)))
#define P_ERROR(msg, ...) fprintf(stderr, "ERROR: "msg, ##__VA_ARGS__)
#define VERSION_MAJOR 1
#define VERSION_MINOR 0
#define VERSION_REVISION 0
#define VERSION_STRING "1.0.0"

// Procedure returning an error and taking any arguments
typedef Error (EProc)();

DEFINE_TYPEDEFS(EProc);

// SDL types
DEFINE_TYPEDEFS(SDL_Window);
DEFINE_TYPEDEFS(SDL_Renderer);
DEFINE_TYPEDEFS(SDL_Texture);
DEFINE_TYPEDEFS(SDL_Surface);
DEFINE_TYPEDEFS(SDL_Rect);
DEFINE_TYPEDEFS(Mix_Chunk);

// **** main.c ****

// state
typedef struct
{
	P_SDL_Window window;
	P_SDL_Renderer renderer;
	U32 height;
	U32 width;
	U32 centerX;
	U32 centerY;
} State;

extern State g_state;

DEFINE_TYPEDEFS(State);

// **** font.c ****

typedef struct
{
	U32 advance;
	P_SDL_Texture tex;
} Glyph;

DEFINE_TYPEDEFS(Char);

typedef struct
{
	U32  maxHeight;
	U32  lineHeight;
	Glyph glyphs[256]; // ASCII table
} Font;

DEFINE_TYPEDEFS(Font);
	
Error FONT_initialize();
Error FONT_finalize();
Error FONT_create(CP_Font f, P_SDL_Renderer r, S32 size, P_Char fName);
Error FONT_destroy(CP_Font f);
Void FONT_renderString(CPC_Font f,
		       P_SDL_Renderer r,
		       U32 x,
		       U32 y,
		       PC_Char s);

// **** intro.c ****

Error INTRO_scene(CPP_EProc ep);

// **** title.c ****

Error TITLE_scene(CPP_EProc ep);

// **** catalog.c ****

typedef enum
{
	// digits
	TID_ZERO,
	TID_ONE,
	TID_TWO,
	TID_THREE,
	TID_FOUR,
	TID_FIVE,
	TID_SIX,
	TID_SEVEN,
	TID_EIGHT,
	TID_NINE,
	// bird
	TID_BIRD_1,
	TID_BIRD_2,
	TID_BIRD_3,
	TID_BIRD_WATER_1,
	TID_BIRD_WATER_2,
	TID_BIRD_WATER_3,
	// cloud
	TID_CLOUD_1,
	TID_CLOUD_2,
	TID_CLOUD_WATER_1,
	TID_CLOUD_WATER_2,
	// buildings
	TID_BUILDINGS_1,
	TID_BUILDINGS_2,
	TID_BUILDINGS_WATER_1,
	TID_BUILDINGS_WATER_2,
	// ground
	TID_GROUND,
	TID_GROUND_BETWEEN,
	TID_GROUND_WATER,
	// column
	TID_COLUMN_BASE,
	TID_COLUMN_UP,
	TID_COLUMN_DOWN,
	TID_COLUMN_BASE_WATER,
	TID_COLUMN_DOWN_WATER,
	// parallax
	TID_PARALLAX_1,
	TID_PARALLAX_2,
	TID_PARALLAX_3,
	TID_PARALLAX_4,
	TID_PARALLAX_5,
	TID_PARALLAX_6,
	TID_PARALLAX_WATER_1,
	TID_PARALLAX_WATER_2,
	TID_PARALLAX_WATER_3,
	TID_PARALLAX_WATER_4,
	TID_PARALLAX_WATER_5,
	TID_PARALLAX_WATER_6,
        // end mark
	TID_MAX,
} TextureID;

typedef enum
{
	SID_PASS,
	SID_MAX
} SoundID;

DEFINE_TYPEDEFS(TextureID);
DEFINE_TYPEDEFS(SoundID);

Error CATALOG_initialize();
Error CATALOG_finalize();
Error CATALOG_renderTexture(C_TextureID tid, C_S32 x, C_S32 y);
Error CATALOG_playSound(C_SoundID sid);

// **** background.c ****

Error BACKGROUND_render(C_S32 offset);

// **** gameplay.c ****

Error GAMEPLAY_scene(CPP_EProc ep);
