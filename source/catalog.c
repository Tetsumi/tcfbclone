/*
  tcfbclone
 
  Contributors:
  Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "base.h"
#include <SDL_image.h>

static P_SDL_Texture s_tex;
static C_SDL_Rect s_tCoords[TID_MAX] = {
	[TID_ZERO]  = {.x = 172, .y = 1, .w = 5, .h = 7},
	[TID_ONE]   = {.x = 178, .y = 1, .w = 5, .h = 7},
	[TID_TWO]   = {.x = 184, .y = 1, .w = 5, .h = 7},
	[TID_THREE] = {.x = 190, .y = 1, .w = 5, .h = 7},
	[TID_FOUR]  = {.x = 196, .y = 1, .w = 5, .h = 7},
	[TID_FIVE]  = {.x = 172, .y = 9, .w = 5, .h = 7},
	[TID_SIX]   = {.x = 178, .y = 9, .w = 5, .h = 7},
	[TID_SEVEN] = {.x = 184, .y = 9, .w = 5, .h = 7},
	[TID_EIGHT] = {.x = 190, .y = 9, .w = 5, .h = 7},
	[TID_NINE]  = {.x = 196, .y = 9, .w = 5, .h = 7},

	[TID_BIRD_1] = {.x = 35, .y = 1, .w = 10, .h = 8},
	[TID_BIRD_2] = {.x = 47, .y = 1, .w = 10, .h = 8},
	[TID_BIRD_3] = {.x = 59, .y = 1, .w = 10, .h = 8},

	[TID_BIRD_WATER_1] = {.x = 35, .y = 10, .w = 10, .h = 4},
	[TID_BIRD_WATER_2] = {.x = 47, .y = 10, .w = 10, .h = 4},
	[TID_BIRD_WATER_3] = {.x = 59, .y = 10, .w = 10, .h = 4},

	[TID_CLOUD_1] = {.x = 70, .y = 1, .w = 15, .h = 9},
	[TID_CLOUD_2] = {.x = 87, .y = 1, .w = 15, .h = 9},

	[TID_CLOUD_WATER_1] = {.x = 70, .y = 11, .w = 15, .h = 4},
        [TID_CLOUD_WATER_2] = {.x = 87, .y = 11, .w = 15, .h = 4},

	[TID_BUILDINGS_1] = {.x = 105, .y = 3, .w = 10, .h = 12},
	[TID_BUILDINGS_2] = {.x = 117, .y = 1, .w = 13, .h = 14},

	[TID_BUILDINGS_WATER_1] = {.x = 105, .y = 16, .w = 10, .h = 6},
	[TID_BUILDINGS_WATER_2] = {.x = 117, .y = 16, .w = 13, .h = 7},

	[TID_GROUND]         = {.x =  1, .y = 20, .w = 40, .h = 7},
	//[TID_GROUND_BETWEEN] = {.x = 42, .y = 19, .w = 40, .h = 4},
	[TID_GROUND_BETWEEN] = {.x = 83, .y = 31, .w = 40, .h = 4},
	[TID_GROUND_WATER]   = {.x = 42, .y = 24, .w = 40, .h = 3},

	[TID_COLUMN_BASE] = {.x =  3, .y = 9, .w = 12, .h = 10},
	[TID_COLUMN_UP]   = {.x =  1, .y = 1, .w = 16, .h =  7},
	[TID_COLUMN_DOWN] = {.x = 18, .y = 1, .w = 16, .h =  7},

	[TID_COLUMN_BASE_WATER] = {.x = 20, .y =  9, .w = 12, .h = 5},
	[TID_COLUMN_DOWN_WATER] = {.x = 18, .y = 15, .w = 16, .h = 4},

	[TID_PARALLAX_1] = {.x =  42, .y =  35, .w = 40, .h = 24},
	[TID_PARALLAX_2] = {.x =  83, .y =  39, .w = 40, .h = 20},
	[TID_PARALLAX_3] = {.x = 124, .y =  43, .w = 40, .h = 16},
	[TID_PARALLAX_4] = {.x =   1, .y =  61, .w = 40, .h = 13},
	[TID_PARALLAX_5] = {.x =  42, .y =  63, .w = 40, .h = 11},
	[TID_PARALLAX_6] = {.x =  83, .y =  65, .w = 40, .h =  9},

	[TID_PARALLAX_WATER_1] = {.x =  42, .y =  83, .w = 40, .h = 12},
	[TID_PARALLAX_WATER_2] = {.x =  83, .y =  83, .w = 40, .h = 10},
        [TID_PARALLAX_WATER_3] = {.x = 124, .y =  83, .w = 40, .h =  8},
	[TID_PARALLAX_WATER_4] = {.x =   1, .y =  75, .w = 40, .h =  7},
	[TID_PARALLAX_WATER_5] = {.x =  42, .y =  75, .w = 40, .h =  6},
	[TID_PARALLAX_WATER_6] = {.x =  83, .y =  75, .w = 40, .h =  5},
};
static P_Mix_Chunk s_sounds[SID_MAX];

static CPC_Char s_soundFiles[SID_MAX] = {
	[SID_PASS] = "./res/pass.wav",
}; 

Error CATALOG_initialize()
{
	assert(g_state.renderer);
	if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
	{
		P_ERROR("IMG_Init: %s\n", IMG_GetError());
		return ERROR_UNKNOWN;
	}
	P_SDL_Surface s = IMG_Load("./res/1b.png");
	if (!s)
	{
		P_ERROR("Couldn't load PNG file\n");
		return ERROR_UNKNOWN;
	}
	s_tex = SDL_CreateTextureFromSurface(g_state.renderer, s);
	if (!s_tex)
	{
		P_ERROR("Couldn't create main texture\n");
		return ERROR_UNKNOWN;
	}
	SDL_FreeSurface(s);
	for (Index i = 0; i < SID_MAX; ++i)
	{
		s_sounds[i] = Mix_LoadWAV(s_soundFiles[i]);
		if (!s_sounds[i])
		{
			P_ERROR("Couldn't load sound file %s\n",
				s_soundFiles[i]);
			return ERROR_UNKNOWN;
		}
	}
	return ERROR_NONE;
}

Error CATALOG_finalize()
{
	if (s_tex)
	{
		SDL_DestroyTexture(s_tex);
		s_tex = NULL;
	}
	for (Index i = 0; i < SID_MAX; ++i)
	{
		if (s_sounds[i])
		{
			Mix_FreeChunk(s_sounds[i]);
			s_sounds[i] = NULL;
		}
	}
	return ERROR_NONE;
}

Error CATALOG_renderTexture(C_TextureID tid, C_S32 x, C_S32 y)
{
	assert(TID_MAX > tid);
	C_SDL_Rect dest = {
		.x = x,
		.y = y,
		.w = s_tCoords[tid].w * 6,
		.h = s_tCoords[tid].h * 6
	};
	SDL_RenderCopy(g_state.renderer,
		       s_tex,
		       &s_tCoords[tid],
		       &dest);
	return ERROR_NONE;
}

Error CATALOG_playSound(C_SoundID sid)
{
	assert(SID_MAX > sid);
	Mix_PlayChannel(-1, s_sounds[sid], 0);
	return ERROR_NONE;
}
