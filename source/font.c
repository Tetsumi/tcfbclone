/*
  tcfbclone
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "base.h"
#include <SDL_ttf.h>

DEFINE_TYPEDEFS(TTF_Font);

Error FONT_initialize()
{
	if(-1 == TTF_Init())
	{
		P_ERROR("TTF_Init: %s\n", TTF_GetError());
		return ERROR_UNKNOWN;
	}
	return ERROR_NONE;
}

Error FONT_finalize()
{
	TTF_Quit();
	return ERROR_NONE;
}

static void cleanupFont (PP_TTF_Font pf)
{
	if (!*pf)
		return;
	TTF_CloseFont(*pf);
	*pf = NULL;
}

/*
  create a font textures set from a font filename.
*/
Error FONT_create(CP_Font f, P_SDL_Renderer r, S32 size, P_Char fName)
{
	assert(r);
	assert(fName && (0 < strlen(fName)));
	assert(f);
	P_TTF_Font tf CLEANUP(cleanupFont) = TTF_OpenFont(fName, size);
	if (!tf)
	{
		P_ERROR("TTF_OpenFont: %s\n", TTF_GetError());
		return ERROR_UNKNOWN;
	}
	
	f->maxHeight = TTF_FontHeight(tf);
	f->lineHeight = TTF_FontLineSkip(tf);
	SDL_Color color = {.r = 255, .g = 255, .b = 255, .a = 255};
	for (Index i = 0; i < 256; ++i)
	{
		if (!TTF_GlyphIsProvided(tf, i))
		{
			f->glyphs[i].tex = NULL;
			continue;
		}
		// advance
		TTF_GlyphMetrics(tf,
				 i,
				 NULL,
				 NULL,
				 NULL,
				 NULL,
				 &f->glyphs[i].advance);
		// pixels
		P_SDL_Surface s = TTF_RenderGlyph_Blended(tf,
							  i,
							  color);
		if (!s)
		{
			P_ERROR("TTF_RenderGlyph_Blended: %s\n",
				TTF_GetError());
			return ERROR_UNKNOWN;	
		}
		P_SDL_Texture t = SDL_CreateTextureFromSurface(r, s);
		if (!t)
		{
			SDL_Log("Couldn't create font texture: %s",
				SDL_GetError());
			return ERROR_UNKNOWN;
		}
		f->glyphs[i].tex = t;
		SDL_FreeSurface(s);
	}		
	return ERROR_NONE;
}

/*
  destroy all textures of a given font.
*/
Error FONT_destroy(CP_Font f)
{
	assert(f);
	for(Index i = 0; i < 256; ++i)
	{
		if(!f->glyphs[i].tex)
			continue;
		SDL_DestroyTexture(f->glyphs[i].tex);
		f->glyphs[i].tex = NULL;
	}
	f->maxHeight = 0;
	f->lineHeight = 0;
	return ERROR_NONE;
}

/*
  Render a string on screen at position (x, y)
*/
Void FONT_renderString(CPC_Font f,
		       P_SDL_Renderer r,
		       U32 x,
		       U32 y,
		       PC_Char s)
{
	assert(f);
	assert(r);
	assert(s); 
	SDL_Rect dR = {.x = x, .y = y};
	for(;'\0' != *s; ++s)
	{
		
		if ('\n' == *s)
		{
			dR.x = x;
			dR.y += f->lineHeight;
			continue;
		}
		P_SDL_Texture t = f->glyphs[*s].tex;
		if (!t)
			continue;
		SDL_QueryTexture(t, NULL, NULL, &dR.w, &dR.h);
		SDL_RenderCopy(r,
			       t,
			       NULL,
			       &dR);
		dR.x += f->glyphs[*s].advance;
	}

}
