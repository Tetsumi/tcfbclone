/*
  tcfbclone
 
  Contributors:
  Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "base.h"

typedef enum
{
	ACTION_NONE,
	ACTION_JUMP,
	ACTION_IDLE
} Action;

DEFINE_TYPEDEFS(Action);

typedef enum
{
	STATUS_JUMPING,
	STATUS_IDLING,
} Status;

DEFINE_TYPEDEFS(Status);

typedef struct
{
	S32 x;
	S32 height;
	Boolean passed;
} Pipe;

DEFINE_TYPEDEFS(Pipe);

#define NEXT_GRAVITY 10
#define GRAVITY (-7.35f / 100.0f)
#define ANIME_NEXT_JUMPING 100
#define ANIME_NEXT_IDLE 225
#define PIPE_GAP 100
#define PIPE_MAX_HEIGHT (320 - (PIPE_GAP + 60))
#define PIPE_AMOUNT 5
#define PIPE_SPACE 200
#define BIRD_X 50


static SDL_Rect s_rBird = {
	.x = BIRD_X,
	.y = 187,
	.w = 60,
	.h = 48
};

static R32 s_yd;
static R32 s_yVelocity;
static U32 s_yStepTimeout;
static U32 s_animeTimeout;
static U32 s_animeNext;
static U32 s_anime;
static R32 s_accel;
static Pipe s_pipes[PIPE_AMOUNT];
static U32 s_pipesTimeout;
static U32 s_score;
static C_TextureID s_animFrames[] = {
	TID_BIRD_1,
	TID_BIRD_2,
	TID_BIRD_1,
	TID_BIRD_3,
};

static Error randPipe(CP_Pipe p)
{
	assert(p); 
	p->height = rand() % PIPE_MAX_HEIGHT;
	return ERROR_NONE;
}

static Error resetPipes()
{
	for (Index i = 0; i < PIPE_AMOUNT; ++i)
	{
		s_pipes[i].x = 640 + (i * PIPE_SPACE);
		s_pipes[i].passed = false;
		randPipe(&s_pipes[i]);		
	}
	return ERROR_NONE;
}

static Error reset()
{
	// reset bird's parameters
	s_rBird.y = 187;
	s_yVelocity = 0.0f;
	s_yStepTimeout = 0;
	s_animeNext = ANIME_NEXT_IDLE;
	s_animeTimeout = 0;
	s_pipesTimeout = 0;
	s_anime = 0;
	s_accel = 0.0f;
	s_yd = 50.0f;
	s_score = 0;
	srand(SDL_GetTicks());
	resetPipes();	
	return ERROR_NONE;
}

static Error updatePipes(C_U32 ticks)
{
	if (!SDL_TICKS_PASSED(ticks, s_pipesTimeout))
		return ERROR_NONE; 
	s_pipesTimeout = ticks + 16;
	for (Index i = 0; i < PIPE_AMOUNT; ++i)
	{
		--s_pipes[i].x;
		if (0 > s_pipes[i].x + 96)
		{			
			randPipe(&s_pipes[i]);
			s_pipes[i].x = s_pipes[0 == i ?
					       PIPE_AMOUNT - 1
					       : i - 1].x + PIPE_SPACE;
			s_pipes[i].passed = false;
		} else if (50 > s_pipes[i].x + 96 && !s_pipes[i].passed)
		{
			++s_score;
			s_pipes[i].passed = true;
			CATALOG_playSound(SID_PASS);
		}
	}
		return ERROR_NONE;
}

static Error renderPipes()
{
	for (Index i = 0; i < PIPE_AMOUNT; ++i)
	{
		for (S32 y = s_pipes[i].height - 66; y > -60; y -= 60)
			CATALOG_renderTexture(TID_COLUMN_BASE,
					      s_pipes[i].x + 12,
					      y);
		CATALOG_renderTexture(TID_COLUMN_DOWN,
				      s_pipes[i].x,
				      s_pipes[i].height - 42);

		for (S32 y = s_pipes[i].height + PIPE_GAP + 42;
		     y < 320;
		     y += 60)
		{
			CATALOG_renderTexture(TID_COLUMN_BASE,
					      s_pipes[i].x + 12,
					      y > 260 ? 260 : y);
		}
		CATALOG_renderTexture(TID_COLUMN_UP,
				      s_pipes[i].x,
				      s_pipes[i].height + PIPE_GAP);
		// water
		C_S32 height = 374
			+ (320 - (s_pipes[i].height + PIPE_GAP))
			* 0.33125f;		
		for (S32 s = height;
		     s >= 374;
		     s -= 30)
		{
			CATALOG_renderTexture(TID_COLUMN_BASE_WATER,
					      s_pipes[i].x + 12,
					      s - 30 < 374 ? 374 : s - 30);
		}
		CATALOG_renderTexture(TID_COLUMN_DOWN_WATER,
				      s_pipes[i].x,
				      height - 30 < 374 ? 380 : height - 24);
	}
	return ERROR_NONE;
}

static Error renderScore()
{
	U32 s = s_score;
	S32 x = 330;
	do
	{
		CATALOG_renderTexture(TID_ZERO + s%10,
				      x,
				      25);
		s /= 10;
		x -= 35;
	}
	while( s > 0);
	return ERROR_NONE;
}

static Error updateBird(C_U32 ticks, C_Action a)
{
	if (ACTION_JUMP == a)
	{
		s_yVelocity = 5.0f;
		s_animeNext = ANIME_NEXT_JUMPING;
		s_animeTimeout = 0;
	}	
	if (SDL_TICKS_PASSED(ticks, s_animeTimeout))
	{
		s_anime = (s_anime + 1) % 4;
		s_animeTimeout = ticks + s_animeNext;
	}	
	if (SDL_TICKS_PASSED(ticks, s_yStepTimeout))
	{
		s_yd -= s_yVelocity * 0.1;
		s_yVelocity += GRAVITY;
		s_rBird.y = s_yd;
		if (0 > s_rBird.y)
			s_rBird.y = 0;
		s_yStepTimeout = ticks + NEXT_GRAVITY;
		if(0 > s_yVelocity)
			s_animeNext = ANIME_NEXT_IDLE;
	}
	return ERROR_NONE;
}

static Boolean over()
{
	for (Index i = 0; i < PIPE_AMOUNT; ++i)
	{
		if (s_pipes[i].x >= BIRD_X
		    && s_pipes[i].x <= BIRD_X + 60
		    && (s_rBird.y < s_pipes[i].height
			|| s_rBird.y + 48 > s_pipes[i].height + PIPE_GAP))
			return true;
	}
	return (s_rBird.y + s_rBird.h >= 320);
}

Error GAMEPLAY_scene(CPP_EProc ep)
{
	assert(ep);
	SDL_Event event = {.type = 0};
	U32 scrollTimeout = SDL_GetTicks() + 16;
	S32 bgOffset = 0; 	
	Action ac;
	CPC_U8 keys = SDL_GetKeyboardState(NULL);
	bool jumped = false;
	reset();
	while (true)
	{
		
		SDL_PollEvent(&event);
		if (event.type == SDL_QUIT)
		{
			*ep = NULL;
			break;
		}
		if (over())
		{
			*ep = TITLE_scene;
			break;
		}
		ac = ACTION_NONE;		
		if (keys[SDL_SCANCODE_SPACE] && !jumped)
		{
			jumped = true;
			ac = ACTION_JUMP;
		}
		else if (!keys[SDL_SCANCODE_SPACE])
			jumped = false;
		C_U32 ticks = SDL_GetTicks();
		if (SDL_TICKS_PASSED(ticks, scrollTimeout))
		{
			scrollTimeout = ticks + 16;
			--bgOffset;
		}
		updateBird(ticks, ac);
		updatePipes(ticks);
		BACKGROUND_render(bgOffset);
		CATALOG_renderTexture(s_animFrames[s_anime],
				      s_rBird.x,
				      s_rBird.y);
		CATALOG_renderTexture(s_animFrames[s_anime] + 3,
				      s_rBird.x,
				      (C_S32)(480 - ((s_rBird.y + s_rBird.h)
						     * 0.33125f)));
		renderPipes();
		renderScore();
		SDL_RenderPresent(g_state.renderer);
	}
	return ERROR_NONE;
}
