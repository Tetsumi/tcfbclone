/*
  tcfbclone
 
  Contributors:
  Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "base.h"

static P_Char s_font = "./res/GeosansLight.ttf";

Error INTRO_scene(CPP_EProc ep)
{
	assert(ep);
	Font font;
	Error e = FONT_create(&font,
			      g_state.renderer,
			      24,
			      s_font);
	if (ERROR_NONE != e)
	{
		P_ERROR("Coulnd't create font %s\n", s_font);
		return ERROR_UNKNOWN;
	}	
	SDL_Event event;
	// Intro has 4 seconds duration
	U32 timeout = SDL_GetTicks() + 4000;
	// render intro
	SDL_SetRenderDrawColor(g_state.renderer, 0, 55, 0, 0);
	SDL_RenderClear(g_state.renderer);	
	// intro text
	FONT_renderString(&font,
			  g_state.renderer,
			  10,
			  10,
			  "Flappy Bird Clone\n"
			  "version " VERSION_STRING "\n"
			  "Art by Lucid Design "
			  "(https://opengameart.org/users/ld)\n"
			  "Flappy Birdy font by Geronimo\n"
			  "Geo Sans Light font by Manfred Klein");
	SDL_RenderPresent(g_state.renderer);
	while (true)
	{
		SDL_PollEvent(&event);
		if (event.type == SDL_QUIT)
		{
			*ep = NULL;
			break;
		}
		// go next scene if timed out or a key has been pressed.
		if (event.type == SDL_KEYDOWN ||
		    SDL_TICKS_PASSED(SDL_GetTicks(), timeout))
		{
			*ep = TITLE_scene;
			break;
		}				
	}
	FONT_destroy(&font);
	return ERROR_NONE;
}
