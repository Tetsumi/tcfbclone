/*
  tcfbclone
 
  Contributors:
     Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "base.h"

// logical size
State g_state = {
	.width = 640,
	.height = 480,
	.centerX = 640 / 2,
	.centerY = 480 / 2
};

Error initializeSDL()
{
	if (SDL_Init(SDL_INIT_VIDEO|SDL_INIT_AUDIO) != 0)
	{
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return ERROR_UNKNOWN;
	}
        C_U32 flags = MIX_INIT_OGG|MIX_INIT_MOD;
	C_U32 initted = Mix_Init(flags);
	if(initted & flags != flags)
	{
	        P_ERROR("Mix_Init: Failed to init required ogg and mod support!\n");
		P_ERROR("Mix_Init: %s\n", Mix_GetError());
	}
	return ERROR_NONE;
}

/*
  initialize the modules then create a window and a rendender.

  modules initialized:
    SDL
    font
    catalog
*/
Error initialize()
{
	Error e = initializeSDL();
	if (ERROR_NONE != e) 
		return e;
	Int ee = SDL_CreateWindowAndRenderer(g_state.width,
					     g_state.height,      
					     SDL_WINDOW_OPENGL |
					     SDL_WINDOW_RESIZABLE,
					     &g_state.window,
					     &g_state.renderer);
	if (-1 == ee)
	{
		SDL_Log("Couldn't create window and renderer: %s",
			SDL_GetError());
		return ERROR_UNKNOWN;
	}
	ee = Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 1024);
	if (-1 == ee)
	{
		P_ERROR("Couldn't open audio: %s\n", Mix_GetError());
		return ERROR_UNKNOWN;
	}
	SDL_SetWindowTitle(g_state.window, "tcfbclone");
        SDL_RenderSetLogicalSize(g_state.renderer, g_state.width, g_state.height);
	e = FONT_initialize();
	if (ERROR_NONE != e) 
		return e;
	e = CATALOG_initialize();
	if (ERROR_NONE != e)
		return e;
	return ERROR_NONE;
}

/*
  does cleaning up.
  used with atexit().
*/
Void finalize()
{
	CATALOG_finalize();
	FONT_finalize();
	if (g_state.window)
	{
		SDL_DestroyRenderer(g_state.renderer);
		SDL_DestroyWindow(g_state.window);
		g_state.window = NULL;
		g_state.renderer = NULL;
	}
	Mix_CloseAudio();
	Mix_Quit();
	SDL_Quit();
}

int main(int argc, char **argv)
{
	Error e = initialize();
	if (ERROR_NONE != e)
	{
		P_ERROR("Initialization failed (%u)\n", e);
		return EXIT_FAILURE;
	}
	atexit(finalize);
	P_EProc ep = INTRO_scene;
	while (ep)
	{
		SDL_PumpEvents();
		SDL_FlushEvents(SDL_FIRSTEVENT, SDL_LASTEVENT);
		ep(&ep);
	}
	return EXIT_SUCCESS;
}
