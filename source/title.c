/*
  tcfbclone
 
  Contributors:
  Tetsumi <tetsumi@vmail.me>

  Copyright (C) 2015 Tetsumi
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
  
  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>. 
*/

#include "base.h"

static P_Char s_font = "./res/FlappyBirdy.ttf";

Error TITLE_scene(CPP_EProc ep)
{
	assert(ep);
	Font font;
	Error e = FONT_create(&font,
			      g_state.renderer,
			      100,
			      s_font);
	if (ERROR_NONE != e)
	{
		P_ERROR("Coulnd't create font %s\n", s_font);
		return ERROR_UNKNOWN;
	}	
	SDL_Event event = {.type = 0};
	U32 timeout = SDL_GetTicks() + 225;
	S32 off = 0;
	while (true)
	{
		SDL_PollEvent(&event);
		if (event.type == SDL_QUIT)
		{
			*ep = NULL;
			break;
		}
		// go next scene if timed out or a key has been pressed.
		if (event.type == SDL_KEYDOWN)
		{
			*ep = GAMEPLAY_scene;
			break;
		}
		// render
		SDL_SetRenderDrawColor(g_state.renderer, 0, 55, 0, 0);
		SDL_RenderClear(g_state.renderer);	
		BACKGROUND_render(off);
		FONT_renderString(&font,
				  g_state.renderer,
				  g_state.centerX - ((13 * font.glyphs['a'].advance) / 2),
				  g_state.centerY - ((font.lineHeight * 4) / 2),
				  " Flappy Bird\n"
				  "   Clone\n\n"
				  "Press any Key");
		SDL_RenderPresent(g_state.renderer);
		U32 cTicks = SDL_GetTicks();
		if (SDL_TICKS_PASSED(cTicks, timeout))
		{
			timeout = cTicks + 16;
		        --off;
		}
	}
	FONT_destroy(&font);
	return ERROR_NONE;
}
